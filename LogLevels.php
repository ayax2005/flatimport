<?php

class LogLevels {
	
	const NOTIFICATION = 'NOTIFICATION';
	const NOTICE = 'NOTICE';
	const WARNING = 'WARNING';
	const ERROR = 'ERROR';
	const FATAL = 'FATAL';
}
