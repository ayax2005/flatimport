<?php

class Mail {

	public function serviceMail($message) {
		$this->send(EMAIL_ADMIN_LIST, LogLevels::FATAL, $message, 'administrator');
	}

	public function send($mail, $subject, $message, $login) {
		$domen = Engine::getInstance()->getSite()->getDomen();
		
		$headers = "Content-type: text/html; charset=utf-8 \r\n";
		$headers .= "From: $domen <admin@$domen>\r\n";
		$message = str_replace('\r\n', '<br/>', $message);
		$message = str_replace('\n', '<br/>', $message);
		$message = "<html>
	<head>
		<title>$subject</title>
	</head>
	<body><h1>".___('mail_greeting').", $login!</h1>
		<h2>$subject</h2>
		$message<br/><br/>
		<p>".___('mail_best_regards').",<br/>".___('mail_signature')." $domen<br/>
		" . date('d-m-Y') . "</p>
	</body>
</html>";

		if(DEBUG_LOG_MAIL) {
			Logger::getInstance()->put("mail for $mail: $message");
		}
		mail($mail, $subject, $message, $headers);
	}

}