<?php

class StorageResult {
	protected $rows = 0;
	protected $data = null;
	
	public function __construct($data, $pk = false) {
		if($data) {
			$this->rows = mysql_num_rows($data);
			
			if($this->rows == 1) {
				$this->data = mysql_fetch_object($data);
			} else {
				while ($row = mysql_fetch_object($data)) {
					if($pk) {
						$this->data[$row->$pk] = $row;
					} else {
						$this->data[] = $row;
					}
					
				}
			}
		}
	}

	public function count() {
		return $this->rows;
	}
	
	public function getData() {
		return $this->data;
	}
	
	public function returnResult() {
		return $this->count() ? $this : false;
	}
}
