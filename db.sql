
DROP TABLE IF EXISTS `regions`;
CREATE TABLE IF NOT EXISTS `regions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `code` varchar(100) NOT NULL,
  `selfid` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `level` int(11) NOT NULL,
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `parent_id` (`parent_id`),
  KEY `level` (`level`),
  KEY `status` (`status`),
  KEY `selfid` (`selfid`),
  KEY `parent_id_2` (`parent_id`,`level`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `osm` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `place_id` int(11) NOT NULL,
  `osm_type` varchar(30) NOT NULL,
  `osm_id` int(11) NOT NULL,
  `place_rank` int(11) NOT NULL,
  `boundingbox` varchar(300) NOT NULL,
  `lat` double NOT NULL,
  `lon` double NOT NULL,
  `display_name` varchar(100) NOT NULL,
  `class` varchar(20) NOT NULL,
  `type` varchar(20) NOT NULL,
  `importance` double NOT NULL,
  `icon` varchar(300) NOT NULL,
  `flats` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `indahouse` (
  `house_id` int(11) NOT NULL,
  `house_num` varchar(10) NOT NULL,
  `street_id` int(11) NOT NULL,
  `street_name` varchar(200) NOT NULL,
  `area_id` int(11) NOT NULL,
  `area_name` varchar(200) NOT NULL,
  `flats_count` int(11) NOT NULL,
  `region_id` int(11) NOT NULL,
  `region_name` varchar(200) NOT NULL,
  PRIMARY KEY (`house_id`),
  KEY `street_id` (`street_id`,`street_name`,`area_id`,`area_name`,`region_id`,`region_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;