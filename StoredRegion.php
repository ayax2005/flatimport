<?php

require_once 'Stored.php';

class StoredRegion extends Stored {

    protected $getAddressList;

    public function __construct() {
        $this->getAddressList = "SELECT
					`" . DB_TABLE . "`.name AS street,
                    `" . DB_TABLE . "`.selfid AS street_id,
                    home.name AS home,
					home.selfid AS home_id,
					flats.cnt AS flats
				FROM `" . DB_TABLE . "`
				LEFT JOIN (
					SELECT * FROM `" . DB_TABLE . "` WHERE level=3
				) AS home ON home.parent_id=`" . DB_TABLE . "`.selfid
				LEFT JOIN (SELECT parent_id, COUNT(*) AS cnt FROM `" . DB_TABLE . "` WHERE level=4
					GROUP BY `" . DB_TABLE . "`.parent_id
				) AS flats ON flats.parent_id = home.selfid
				WHERE `" . DB_TABLE . "`.parent_id=$1
				ORDER BY street, home";

        $this->getFlats = "SELECT parent_id AS home, COUNT(*) AS cnt
				FROM `" . DB_TABLE . "`
				WHERE level=4
				GROUP BY parent_id";

        $this->getAreas = "SELECT *
				FROM `" . DB_TABLE . "`
				WHERE level=1";

        $this->getStreets = "SELECT *
				FROM `" . DB_TABLE . "`
				WHERE level=2";
    }

}
