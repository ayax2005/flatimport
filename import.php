<?php

$timeStart = time();

require_once 'conf.php';
require_once 'ImportIterator.php';

Storage::getInstance()->remove(DB_TABLE, array());

$level = 0;
$id = 'path_';

$iterator = new ImportIterator();
$iterator->getRootLevel($id, $level);

$searchItem = Storage::getInstance()->get(DB_TABLE, array('name' => SEARCH_REGION));
echo 'Сканируется '.SEARCH_REGION.PHP_EOL;
$iterator->getByParent($searchItem->selfid, $level);

$time = time() - $timeStart;
echo PHP_EOL.'Данные импортированы за '.$time.' секунд.'.PHP_EOL.PHP_EOL;