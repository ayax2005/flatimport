<?php

require_once 'conf.php';
require_once 'ImportIterator.php';

Storage::getInstance()->remove(OSM_TABLE, array());
$areas = Storage::getInstance()->execute('Region', 'getAreas', array(), 'selfid');
$areasData = $areas->getData();

$streets = Storage::getInstance()->execute('Region', 'getStreets', array(), 'selfid');
$streetsData = $streets->getData();

$homes = Storage::getInstance()->getList(DB_TABLE, array('level' => 3));

$flats = Storage::getInstance()->execute('Region', 'getFlats', array(), 'home');
$flatsData = $flats->getData();

$iterator = new ImportIterator();

foreach ($homes as $home) {
	$item = new AddressModel();
	$item->state = SEARCH_REGION;
	$item->home = $home->name;

	$item->flats = $flatsData[$home->selfid]->cnt;

	$street = $streetsData[$home->parent_id];
	$item->street = $street->name;
	$area = $areasData[$street->parent_id];
	$item->area = $area->name;
	
	//http://nominatim.openstreetmap.org/search?q=paris&format=xml
	
}