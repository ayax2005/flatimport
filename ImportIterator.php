<?php

require_once 'Curler.php';
require_once 'ItemModel.php';
require_once 'AddressModel.php';

class ImportIterator {
	
	protected $curler;
	protected $deepLevel = 5;


	public function __construct() {
		$this->curler = new Curler();
	}

	public function getRootLevel($id, $level) {
		$json = $this->curler->getData($level, $id);
		$data = json_decode($json);
		
		if(count($data)) {
			$this->saveData($data, $id, $level);
			return true;
		} else {
			return false;
		}
	}
	
	public function getByParent($id, $level) {
		$level++;
		
		return $this->isLastLevel($level) ? false : $this->getRootLevel($id, $level);
	}
	
	public function getChildList($parentId){	
		return Storage::getInstance()->getList(DB_TABLE, array('parent_id' => $parentId));
	}
	
	public function getAddressList($parentId) {
		return Storage::getInstance()->execute('Region', 'getAddressList', array($parentId));
	}

	protected function saveData($data, $id, $level) {
		foreach ($data as $item) {
			$im = new ItemModel();

			$im->name = $item->data->title;
			$im->code = $item->attr->addrId;
			$im->selfid = $item->attr->id;
			$im->parent_id = $id;
			$im->level = $level;
			
			$this->logProgress($im);

			if(!$level) {
				$im->status = 0;
			} elseif(!$this->getByParent($im->selfid, $level)) {
				$im->status = 1;
			}

			$im->save();
		}
	}
	
	protected function isLastLevel($level) {
		return $level >= $this->deepLevel;
	}
	
	protected function logProgress(ImportModel $im) {
		if($im->level && ($im->level < ($this->deepLevel - 1))) {
			echo $im>name.PHP_EOL;
		} elseif($this->isLastLevel($im->level)) {
			echo '.';
		} else {
			echo '|';
		}
	}
}
