<?php

require_once 'Logger.php';
require_once 'StoredRegion.php';
require_once 'StorageResult.php';

define('DEBUG_LOGGER_SWITCH_ON', true); 
define('DEBUG_LOG_MYSQL_QUERRIES', true);
define('DEBUG_CLEAR_LOGS_FOR_RUN', true);
define('DEBUG_EXPLODE_LOGS_BY_DAY', false);
define('PATH_LOGS', 'logs/');

class Storage {

	protected static $instance = null;

	protected function __construct() {
		if (!($connection = mysql_connect(DB_LOCATION, DB_USER, DB_PASSWORD))) {
			Logger::getInstance()->put("db host don't response", LogLevels::FATAL);
		}

		if (!mysql_select_db(DB_SCHEMA, $connection)) {
			Logger::getInstance()->put("db schema not finded", LogLevels::FATAL);
		}
		mysql_query("set names utf8", $connection);
	}

	public static function getInstance() {
		if (is_null(self::$instance)) {
			self::$instance = new self;
		}

		return self::$instance;
	}

	public function get($table, $where = array()) {
		$where = $this->prepereCondition($where);
		$query = "SELECT * FROM `$table` $where LIMIT 1";

		$this->log($query);

		$data = mysql_query($query);
		if ($data && mysql_num_rows($data)) {
			return mysql_fetch_object($data);
		}

		return false;
	}
	
	public function getList($table, $where = array()) {
		$result = false;
		$where = $this->prepereCondition($where);
		$query = "SELECT * FROM `$table` $where";

		$this->log($query);

		$data = mysql_query($query);
		if ($data) {
			while($f = mysql_fetch_object($data)) {
				$result[] = $f;
			}
		}
		
		return $result;
	}

	public function add($table, $embed) {
		$query = "INSERT INTO `$table` ";

		$keys = $vals = array();
		foreach ($embed as $key => $val) {
			$keys[] = "`$key`";
			$vals[] = "'$val'";
		}

		$keys = implode(', ', $keys);
		$vals = implode(', ', $vals);

		$query .= "($keys) VALUES ($vals)";

		$this->log($query);

		mysql_query($query);
		return mysql_insert_id();
	}

	public function update($table, $updates, $where) {
		$settes = array();

		foreach ($updates as $key => $val) {
			$settes[] = "`$key` = '$val'";
		}
		$settes = implode(', ', $settes);

		$where = $this->prepereCondition($where);
		
		$query = "UPDATE `$table` SET $settes $where";
		$this->log($query);

		mysql_query($query);
	}

	public function replace($table, $updates) {
		$settes = array();

		foreach ($updates as $key => $val) {
			$settes[] = "`$key` = '$val'";
		}
		$settes = implode(', ', $settes);

		$query = "INSERT INTO `$table` SET $settes ON DUPLICATE KEY UPDATE $settes";
		$this->log($query);

		mysql_query($query);
	}

	public function findOne($storedClass, $method, $condition) {
		$where = $this->prepereCondition($condition);
		$storedClassName = 'Stored' . $storedClass;
		$stored = new $storedClassName();

		$query = $stored->getQuery($method) . $where;
		$this->log($query);

		$data = mysql_query($query);
		if ($data && mysql_num_rows($data)) {
			return mysql_fetch_object($data);
		}

		return false;
	}
	
	public function execute($storedClass, $method, $params = array(), $pk = false) {
		$storedClassName = 'Stored' . $storedClass;
		$stored = new $storedClassName();
		
		$query = $stored->getQuery($method);
		
		foreach($params as $key => $val) {
			$query = str_replace('$'.($key+1), $val, $query);
		}
		$this->log($query);

		$data = mysql_query($query);
		$result = new StorageResult($data, $pk);

		return $result->returnResult();
	}
	
	public function remove($table,$condition) {
		$where = $this->prepereCondition($condition);
		
		$query = "DELETE FROM `$table` $where";
		$this->log($query);
		
		mysql_query($query);
	}

	public function prepereCondition($where) {
		$result = '';

		if (isset($where['mc']) && in_array($where['mc'], array('OR', 'AND'))) {
			$mainCondition = $where['mc'];
			unset($where['mc']);
		} else {
			$mainCondition = 'AND';
		}

		if (count($where)) {
			$preResult = array();

			foreach ($where as $column => $condition) {
				$tempCondition = " `$column` ";

				if (is_array($condition)) {
					if (is_bool($condition)) {
						$condition[0] = (int) $condition[0];
					}

					$tempCondition .= $condition[1] . " '" . $condition[0] . "' ";
				} else {
					if (is_bool($condition)) {
						$condition = (int) $condition;
					}

					$tempCondition .= " = '" . $condition . "'";
				}

				$preResult[] = $tempCondition;
			}

			$result = ' WHERE ' . implode(" $mainCondition ", $preResult);
		}

		return $result;
	}

	protected function log($query) {
		if (DEBUG_LOG_MYSQL_QUERRIES) {
			Logger::getInstance()->put($query);
		}
	}

}
