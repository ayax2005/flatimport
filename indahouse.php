<?php
$time = time();
require_once 'conf.php';
require_once 'ImportIterator.php';

Storage::getInstance()->remove('indahouse', array());

$iterator = new ImportIterator();
$searchItem = Storage::getInstance()->get(DB_TABLE, array('name' => SEARCH_REGION));
$areaList = $iterator->getChildList($searchItem->selfid);

$result = array();
foreach ($areaList as $item) {
	$areaData = $iterator->getAddressList($item->selfid);
	$temp = array($item, $areaData);
	$result[] = $temp;
}
?><!doctype html>
<html>
	<head>
		<meta charset="UTF-8" />
		<title>Заполненение indahouse</title>
	</head>
	<body>
		<h2>Регион <?php echo SEARCH_REGION; ?></h2>
		<div id='areaList'>
			<?php
			echo $time = time() - $time;

			$region_id = $searchItem->selfid;
			$region_name = SEARCH_REGION;

			foreach ($result as $item) {
				$area = $item[0];
				$data = $item[1];

				$area_name = $area->name;
				$area_id = $area->selfid;

				foreach ($data->getData() as $ad) {
					$street_name = $ad->street;
					$street_id = $ad->street_id;
					$house_num = $ad->home;
					$house_id = $ad->home_id;
					$flats_count = $ad->flats;

					$insert = array(
						'area_name' => $area_name,
						'area_id' => $area_id,
						'street_name' => $street_name,
						'street_id' => $street_id,
						'house_num' => $house_num,
						'house_id' => $house_id,
						'flats_count' => $flats_count,
						'region_id' => $region_id,
						'region_name' => $region_name,
					);

					Storage::getInstance()->add('indahouse', $insert);
				}
			}

			echo '<br/>';
			echo $time = time() - $time;
			?>
		</div>
	</body>
</html>