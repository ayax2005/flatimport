<?php
require_once 'conf.php';
require_once 'ImportIterator.php';

$iterator = new ImportIterator();
$searchItem = Storage::getInstance()->get(DB_TABLE, array('name' => SEARCH_REGION));
$areaList = $iterator->getChildList($searchItem->selfid);

$parent_id = isset($_GET['area']) ? (int) $_GET['area'] : 0;
if ($parent_id) {
    $oldStreet = null;
    $searchArea = Storage::getInstance()->get(DB_TABLE, array('selfid' => $parent_id));
    $areaData = $iterator->getAddressList($parent_id);
}
?><!doctype html>
<html>
    <head>
        <meta charset="UTF-8" />
        <title>Адреса</title>
    </head>
    <body>
        <h2>Регион <?php echo SEARCH_REGION; ?></h2>
        <div id='areaList'>
            <form>
                <label>Выберите район</label>
                <select name='area'>
                    <?php foreach ($areaList as $item) { ?>
                        <option value="<?php echo $item->selfid; ?>" <?php if ($item->selfid == $parent_id) { ?>selected="selected"<?php } ?>><?php echo $item->name; ?></option>
                    <?php } ?>
                </select>
                <input type="submit"/>
            </form>
            <?php if ($parent_id) { ?>
                <h3><?php echo $searchArea->name; ?></h3>
                <table>
                    <thead>
                        <tr>
                            <th>Улица</th>
                            <th>Дом</th>
                            <th>Квартиры</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($areaData->getData() as $item) { ?>
                            <tr>
                                <td><?php
                                    if ($item->street != $oldStreet) {
                                        echo $oldStreet = $item->street;
                                    }
                                    ?></td>
                                <td><?php echo $item->home; ?></td>
                                <td><?php echo (int) $item->flats; ?></td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            <?php } ?>
        </div>
    </body>
</html>