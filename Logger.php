<?php

require_once 'LogLevels.php';
require_once 'Mail.php';

class Logger {
	
	protected static $instance = null;
	protected $writeRule = 'a+';
	protected $fileName = 'event.log';
	protected $handle = null;

	protected function __construct() {
		if(DEBUG_LOGGER_SWITCH_ON) {
			if(DEBUG_CLEAR_LOGS_FOR_RUN) {
				$this->writeRule = 'w+';
			}

			if(DEBUG_EXPLODE_LOGS_BY_DAY) {
				$this->fileName = date('y-n-j').'.log';
			}

			$this->handle = fopen(PATH_LOGS.$this->fileName, $this->writeRule);
		}
	}
	
	public static function getInstance() {
		if (is_null(self::$instance)) {
			self::$instance = new self;
		}

		return self::$instance;
	}
	
	public function put($mesage, $logLevel = LogLevels::NOTIFICATION) {
		if(DEBUG_LOGGER_SWITCH_ON) {
			$eventMessage = date('Y-m-d H:i:s')."\t$logLevel\t$mesage".PHP_EOL;
			fwrite($this->handle, $eventMessage);
		}
		
		if($logLevel == LogLevels::FATAL) {
			$mailer = new Mail();
			$mailer->serviceMail($mesage);
		}
	}
	
	public function stat() {
		$embed = array(
			'ip' => $_SERVER['REMOTE_ADDR'],
			'agent' => $_SERVER['HTTP_USER_AGENT'],
			'alias' => Engine::getInstance()->getPage()->getAlias(),
			'user_level' => Engine::getInstance()->getUser()->getLevel(),
			'user_id' => Engine::getInstance()->getUser()->getId(),
		);
		
		Storage::getInstance()->add('visit_stat', $embed);
	}
}
