<?php

require_once 'Storage.php';

class ItemModel {
	public $name;
	public $code;
	public $selfid;
	public $parent_id;
	public $level;
	public $status;
	
	public function save() {
		Storage::getInstance()->add(DB_TABLE, (array)$this);
	}
}
